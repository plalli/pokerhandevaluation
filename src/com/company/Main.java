package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Card> hand1 = new ArrayList<>();
        for(int i=0; i < 5; i++) {
            Card card = new Card(args[i]);
            hand1.add(card);
        }
        CardHelper.sortHand(hand1);
        ArrayList<Card> hand2 = new ArrayList<>();
        for(int i=5; i < 10; i++) {
            Card card = new Card(args[i]);
            hand2.add(card);
        }
        CardHelper.sortHand(hand2);

        System.out.println("Hand 1 (sorted):");
        for(Card card : hand1) {
            System.out.print(card.getRank() + card.getSuit() + " ");
        }
        System.out.println("Score:" + Evaluator.score(hand1));

        System.out.println();
        System.out.println("Hand 2 (sorted):");
        for(Card card : hand2) {
            System.out.print(card.getRank() + card.getSuit() + " ");
        }
        System.out.println("Score:" + Evaluator.score(hand2));

        if(Evaluator.winningHand(hand1, hand2) == 0) {
            System.out.println("It is a draw!");
        }
        else {
            System.out.println("The winning hand is Hand " + Evaluator.winningHand(hand1, hand2));
        }
    }
}
