package com.company;

import java.util.ArrayList;

public class Evaluator {

    public static int winningHand(ArrayList<Card> hand1, ArrayList<Card> hand2) {
        int hand1Score = score(hand1);
        int hand2Score = score(hand2);

        if(hand1Score > hand2Score) {
            return 1;
        }
        else if (hand2Score > hand1Score){
            return 2;
        }
        else {
            return tieBreakerDecider(hand1Score, hand1, hand2);
        }
    }

    public static int score(ArrayList<Card> hand) {
        if(isFlush(hand) && isStraight(hand)) {
            return 9;
        }
        else if(isFourOfAKind(hand)) {
            return 8;
        }
        else if(isFullHouse(hand)) {
            return 7;
        }
        else if(isFlush(hand)) {
            return 6;
        }
        else if(isStraight(hand)) {
            return 5;
        }
        else if(isThreeOfAKind(hand)) {
            return 4;
        }
        else if(isTwoPair(hand)) {
            return 3;
        }
        else if(isOnePair(hand)) {
            return 2;
        }
        else {
            return 1;
        }
    }

    public static int tieBreakerDecider(int score, ArrayList<Card> hand1, ArrayList<Card> hand2) {
        switch(score) {
            case 9: return straightTieBreaker(hand1, hand2);
            case 8: return fourOfAKindTieBreaker(hand1, hand2);
            case 7: return fullHouseTieBreaker(hand1, hand2);
            case 6: return flushTieBreaker(hand1, hand2);
            case 5: return straightTieBreaker(hand1, hand2);
            case 4: return threeOfAKindTieBreaker(hand1, hand2);
            case 3: return twoPairTieBreaker(hand1, hand2);
            case 2: return onePairTieBreaker(hand1, hand2);
            case 1: return highCardTieBreaker(hand1, hand2);
        }
        // Failure case for now
        return 0;
    }

    public static boolean isFlush(ArrayList<Card> hand) {
        String suit = hand.get(0).getSuit();
        for(int i = 1; i < 5; i++) {
            if(!hand.get(i).getSuit().equals(suit)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isFullHouse(ArrayList<Card> hand) {
        // 0 0 X 0 0
        // 3 of a kind must have something in the X position so we use it as the position
        String rank = hand.get(2).getRank();
        int counter = 0;
        for(int i = 0; i < 5; i++) {
            if(hand.get(i).getRank().equals(rank)) {
                counter++;
            }
        }
        if(counter != 3) {
            return false;
        }
        // X X 0 0 0
        // 0 0 0 X X
        // Pair must be on either side of three of a kind
        if(hand.get(0).getRank().equals(hand.get(1)) && !hand.get(1).getRank().equals(hand.get(2).getRank()) ||
                hand.get(4).getRank().equals(hand.get(3)) && !hand.get(3).getRank().equals(hand.get(2).getRank())) {
            return true;
        }
        return false;
    }

    public static boolean isStraight(ArrayList<Card> hand) {
        // A, 5, 4, 3, 2 needs to be checked because A can also be 1
        if(hand.get(0).getRank().equals("A") && hand.get(1).getRank().equals("5") && hand.get(2).getRank().equals("4")
                && hand.get(3).getRank().equals("3") && hand.get(4).getRank().equals("2")) {
            return true;
        }

        String rank = hand.get(0).getRank();
        int position = CardHelper.Ranks.indexOf(rank);
        for(int i = 1; i < 5; i++) {
            if(!hand.get(i).getRank().equals(CardHelper.Ranks.get(position + 1))) {
                return false;
            }
            position++;
            if(position == 14) {
                position = 0;
            }
        }
        return true;
    }

    public static boolean isFourOfAKind(ArrayList<Card> hand) {
        // 0 X 0 0 0
        // 4 of a kind must have something in the X so we can use it as the position
        String rank = hand.get(1).getRank();
        int counter = 0;
        for(int i = 0; i < 5; i++) {
            if(hand.get(i).getRank().equals(rank)) {
                counter++;
            }
        }
        if(counter > 3) {
            return true;
        }
        return false;
    }

    public static boolean isThreeOfAKind(ArrayList<Card> hand) {
        // 0 0 X 0 0
        // 3 of a kind must have something in the X position so we use it as the position
        String rank = hand.get(2).getRank();
        int counter = 0;
        for(int i = 0; i < 5; i++) {
            if(hand.get(i).getRank().equals(rank)) {
                counter++;
            }
        }
        if(counter > 2) {
            return true;
        }
        return false;
    }

    public static boolean isTwoPair(ArrayList<Card> hand) {
        // Hand is sorted so there's a few possibilities
        // 1 1 2 2 X
        // 1 1 X 2 2
        // X 1 1 2 2
        if(hand.get(0).getRank().equals(hand.get(1).getRank())
            && hand.get(2).getRank().equals(hand.get(3).getRank())) {
            return true;
        }
        if(hand.get(0).getRank().equals(hand.get(1).getRank())
                && hand.get(3).getRank().equals(hand.get(4).getRank())) {
            return true;
        }
        if(hand.get(1).getRank().equals(hand.get(2).getRank())
                && hand.get(3).getRank().equals(hand.get(4).getRank())) {
            return true;
        }
        return false;
    }

    public static boolean isOnePair(ArrayList<Card> hand) {
        // Any card next to it has to be a copy
        for(int i = 0; i < 4; i++) {
            if(hand.get(i).getRank().equals(hand.get(i + 1).getRank())) {
                return true;
            }
        }
        return false;
    }

    public static int onePairTieBreaker(ArrayList<Card> hand1, ArrayList<Card> hand2) {
        for(int i = 0; i < 5; i++) {
            // If they are equal, it is either the pair or an equivalent kicker
            if(hand1.get(i).getRankValue() > hand2.get(i).getRankValue()) {
                return 1;
            }
            if(hand2.get(i).getRankValue() > hand1.get(i).getRankValue()) {
                return 2;
            }
        }
        return 0;
    }

    public static int twoPairTieBreaker(ArrayList<Card> hand1, ArrayList<Card> hand2) {
        // 1 1 2 2 X
        // 1 1 X 2 2
        // X 1 1 2 2
        // Kicker can be any X, since hands are sorted we can look through
        for(int i = 0; i < 5; i++) {
            // If they are equal, it is either the 2 pair or an equivalent kicker
            if(hand1.get(i).getRankValue() > hand2.get(i).getRankValue()) {
                return 1;
            }
            if(hand2.get(i).getRankValue() > hand1.get(i).getRankValue()) {
                return 2;
            }
        }
        return 0;
    }

    public static int threeOfAKindTieBreaker(ArrayList<Card> hand1, ArrayList<Card> hand2) {
       // Kicker can be any other card
        for(int i = 0; i < 5; i++) {
            // If they are equal, it is either the 3 of a kind card or an equivalent kicker
            if(hand1.get(i).getRankValue() > hand2.get(i).getRankValue()) {
                return 1;
            }
            if(hand2.get(i).getRankValue() > hand1.get(i).getRankValue()) {
                return 2;
            }
        }
        return 0;
    }

    public static int fourOfAKindTieBreaker(ArrayList<Card> hand1, ArrayList<Card> hand2) {
        // 0 X 0 0 0
        // X must have one of the 4 of a kind numbers therefore we can always check it to see highest value
        if (hand1.get(1).getRankValue() > hand2.get(1).getRankValue()) {
            return 1;
        }
        if (hand2.get(1).getRankValue() > hand1.get(1).getRankValue()) {
            return 2;
        }
        // Kicker can be on either side
        int hand1Kicker = hand1.get(0).getRank().equals(hand1.get(1).getRank()) ?
                hand1.get(4).getRankValue() : hand1.get(0).getRankValue();
        int hand2Kicker = hand2.get(0).getRank().equals(hand2.get(1).getRank()) ?
                hand2.get(4).getRankValue() : hand2.get(0).getRankValue();
        if (hand1Kicker > hand2Kicker) {
            return 1;
        }
        if (hand2Kicker > hand1Kicker) {
            return 2;
        }
        return 0;
    }

    public static int straightTieBreaker(ArrayList<Card> hand1, ArrayList<Card> hand2) {
        // Kicker can be any other card
        for(int i = 0; i < 5; i++) {
            if(hand1.get(i).getRankValue() > hand2.get(i).getRankValue()) {
                return 1;
            }
            if(hand2.get(i).getRankValue() > hand1.get(i).getRankValue()) {
                return 2;
            }
        }
        return 0;
    }

    public static int fullHouseTieBreaker(ArrayList<Card> hand1, ArrayList<Card> hand2) {
        // Could be 3 of a kind first, could be pair
        // We can check middle card to see largest 3 of a kind
        if(hand1.get(2).getRankValue() > hand2.get(2).getRankValue()) {
            return 1;
        }
        if(hand2.get(2).getRankValue() > hand1.get(2).getRankValue()) {
            return 2;
        }
        for(int i = 0; i < 5; i++) {
            if(hand1.get(i).getRankValue() > hand2.get(i).getRankValue()) {
                return 1;
            }
            if(hand2.get(i).getRankValue() > hand1.get(i).getRankValue()) {
                return 2;
            }
        }
        return 0;
    }

    public static int flushTieBreaker(ArrayList<Card> hand1, ArrayList<Card> hand2) {
        // Kicker can be any other card
        for(int i = 0; i < 5; i++) {
            if(hand1.get(i).getRankValue() > hand2.get(i).getRankValue()) {
                return 1;
            }
            if(hand2.get(i).getRankValue() > hand1.get(i).getRankValue()) {
                return 2;
            }
        }
        return 0;
    }

    public static int highCardTieBreaker(ArrayList<Card> hand1, ArrayList<Card> hand2) {
        // Cards are sorted, return first biggest seen
        for(int i = 0; i < 5; i++) {
            if (hand1.get(i).getRankValue() > hand2.get(i).getRankValue()) {
                return 1;
            }
            if (hand2.get(i).getRankValue() > hand1.get(i).getRankValue()) {
                return 2;
            }
        }
        return 0;
    }

}
