package com.company;

public class Card {
    private String suit;
    private String rank;

    public Card(String card) {
        for(String r : CardHelper.Ranks) {
            if(card.contains(r)) {
                setRank(r);
            }
        }
        for(String i : CardHelper.Suits) {
            if(card.contains(i)) {
                setSuit(i);
            }
        }
    }

    public String getSuit() {
        return suit;
    }

    public void setSuit(String suit) {
        this.suit = suit;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getRankValue() {
        return CardHelper.getRankAsNumber(this.rank);
    }
}
