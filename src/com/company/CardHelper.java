package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class CardHelper {
    public static ArrayList<String> Ranks = new ArrayList<>(Arrays.asList("A", "K", "Q", "J", "10", "9", "8", "7", "6",
            "5", "4", "3", "2"));
    public static ArrayList<String> Suits = new ArrayList<>(Arrays.asList("S", "C", "H", "D"));

    public static int getRankAsNumber(Card card) {
        return 14 - Ranks.indexOf(card.getRank());
    }

    public static int getRankAsNumber(String rank) {
        return 14 - Ranks.indexOf(rank);
    }

    public static int rankValue(String rank) {
        if(rank.equals("A")) {
            return 14;
        }
        else if(rank.equals("K")) {
            return 13;
        }
        else if(rank.equals("Q")) {
            return 12;
        }
        else if(rank.equals("J")) {
            return 11;
        }
        else {
            return Integer.parseInt(rank);
        }

    }

    public static void sortHand(ArrayList<Card> hand) {
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 4 - i; j++) {
                if (rankValue(hand.get(j).getRank()) < rankValue(hand.get(j + 1).getRank())) {
                    Card temp = hand.get(j);
                    hand.set(j, hand.get(j + 1));
                    hand.set(j + 1, temp);
                }
            }
        }
    }
}
