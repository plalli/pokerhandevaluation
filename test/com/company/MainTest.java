package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

class MainTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private static String DRAW = "It is a draw!";
    private static String HAND1_WINS = "The winning hand is Hand 1";
    private static String HAND2_WINS = "The winning hand is Hand 2";

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void STRAIGHT_DRAW() {
        String args[] = new String[] {"KS", "QD", "JC", "10S", "9S", "KH", "QS", "JH", "10D", "9H"};
        Main.main(args);
        assertTrue(outContent.toString().contains(DRAW));
    }

    @Test
    public void STRAIGHT_HAND1_WINS_WITH_BETTER_STRAIGHT() {
        String args[] = new String[] {"KS", "QD", "JC", "10S", "AS", "KH", "QS", "JH", "10D", "9H"};
        Main.main(args);
        assertTrue(outContent.toString().contains(HAND1_WINS));
    }
    @Test
    public void STRAIGHT_HAND2_WINS_WITH_BETTER_STRAIGHT() {
        String args[] = new String[] {"KS", "QD", "JC", "10S", "9S", "KH", "QS", "JH", "10D", "AH"};
        Main.main(args);
        assertTrue(outContent.toString().contains(HAND2_WINS));
    }

    @Test
    public void STRAIGHT_HAND2_WINS_WITH_STRAIGHT_HAND1_WITH_HIGH_CARD() {
        String args[] = new String[] {"KS", "QD", "JC", "10S", "8S", "KH", "QS", "JH", "10D", "AH"};
        Main.main(args);
        assertTrue(outContent.toString().contains(HAND2_WINS));
    }

    @Test
    public void STRAIGHT_HAND1_WINS_WITH_FLUSH_HAND2_WITH_STRAIGHT() {
        String args[] = new String[] {"KS", "QS", "JS", "10S", "8S", "KH", "QS", "JH", "10D", "AH"};
        Main.main(args);
        assertTrue(outContent.toString().contains(HAND1_WINS));
    }

    @Test
    public void STRAIGHT_HAND2_WINS_WITH_FULL_HOUSE_HAND1_WITH_TWO_PAIR() {
        String args[] = new String[] {"KS", "KH", "JS", "JC", "8S", "3H", "3S", "2H", "2D", "2C"};
        Main.main(args);
        assertTrue(outContent.toString().contains(HAND2_WINS));
    }

    @Test
    public void STRAIGHT_HAND2_WINS_AWKWARD_ACE_CASE() {
        String args[] = new String[] {"KS", "KH", "JS", "JC", "8S", "2H", "3S", "4H", "5D", "AC"};
        Main.main(args);
        assertTrue(outContent.toString().contains(HAND2_WINS));
    }

}